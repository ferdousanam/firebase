<?php

namespace App\Http\Controllers;

use Firebase\Auth\Token\Exception\InvalidToken;
use Illuminate\Http\Request;
use Kreait\Firebase\Auth;

class FirebaseAuthController extends Controller
{
    private $auth;

    public function __construct(Auth $auth)
    {
        $this->auth = $auth;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $userData = [
            'displayName' => 'Ferdous Anam',
            'email' => 'anam@gmail.com',
            'password' => '123456',
            'phoneNumber' => '+8801738238012',
        ];

        $user = $this->auth->createUser($userData);
        dd($user->jsonSerialize());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = $this->auth->signInWithEmailAndPassword('anam@gmail.com', '123456');
        dd($user->data());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $uid = $id;
        $number = '+8801738238012';
        $user = $this->auth->getUser($uid);

        if ($number == $user->providerData[0]->phoneNumber) {
            return response()->json(['success' => true]);
        }
        return response()->json(['success' => false]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function verifyToken(Request $request)
    {
        $idTokenString = $request->input('token');
        try {
            $signInResult = $this->auth->signInWithRefreshToken($idTokenString);
        } catch (\InvalidArgumentException $e) {
            echo 'The token could not be parsed: '.$e->getMessage();
        } catch (InvalidToken $e) {
            echo 'The token is invalid: '.$e->getMessage();
        }

        $uid = $signInResult->data()['user_id'];
        $user = $this->auth->getUser($uid);
        return $user;
    }
}
